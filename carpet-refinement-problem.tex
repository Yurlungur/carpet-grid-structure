% Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
% Time-stamp: <2016-04-17 04:04:53 (jmiller)>

\documentclass[nofootinbib, superscriptaddress, ]{revtex4-1}
%\documentclass{article}

\usepackage[colorlinks,citecolor=blue]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{mathrsfs}
\usepackage{latexsym}
\usepackage{mathrsfs}
\usepackage{ragged2e}
\usepackage{multirow}
\usepackage{amsbsy}
\usepackage{siunitx}
\usepackage{listings}
\usepackage{fullpage}

\begin{document}
\begin{titlepage}
\author{Jonah Miller}
\affiliation{Department of Physics, University of Guelph, Guelph, ON,
  Canada}
\affiliation{Perimeter Institute for Theoretical Physics, Waterloo,
  ON, Canada}
\email{jmiller@perimeterinstitute.ca}

\title{Refinement in Carpet and Problems With Visualization}

\begin{abstract}
  In this article, I briefly discuss a particular example refinement
  situation in the Carpet AMR framework. I also discuss the problems
  I am having reading this situation into yt.
\end{abstract}

\maketitle
\end{titlepage}

\section{The Toy Problem}
\label{sec:problem}

Consider a cubic domain, with
\begin{equation}
  \label{eq:domain}
  \begin{aligned}
    x_{min} &= y_{min} = z_{min} = 0\\
    x_{max} &= y_{max} = z_{max} = 240\\
    \Delta x &= \Delta y = \Delta z = 8
  \end{aligned}
\end{equation}
for the coarsest grid. There are four refinement levels, under
cell-centred refinement,\footnote{One can set Carpet to use either
  cell-centred or vertex-centred refinement. I focus on cell-centred
  for now.} each starting at $x=y=z=0$ and extending to a radius of
half of the radius of the parent grid. We refine by a factor of
two. In other words, the first grid level would have
\begin{equation}
  \label{eq:domain}
  \begin{aligned}
    x_{min} &= y_{min} = z_{min} = 0\\
    x_{max} &= y_{max} = z_{max} = 120\\
    \Delta x &= \Delta y = \Delta z = 4
  \end{aligned}
\end{equation}
given that cell edges must align. Each grid has a layer of ghost cells
3 layers deep around it. Figure \ref{fig:nested:domains} shows the
refinement levels on the domain. Note that the axes are not set
correctly. (Note that figure \ref{fig:nested:domains} is a slice
plot. This will be relevant later.)

\begin{figure}[b!]
  \centering
  \includegraphics[width=\columnwidth]{grid_level_slice}
  \caption{The nested grids in our toy refinement problem. There are
    five grid levels, each nested within the parent. This is a
    sliceplot.}
  \label{fig:nested:domains}
\end{figure}

\section{The Refinement Situation}
\label{sec:situation}

Here I describe the refinement situation and define the language used
by Carpet. As one might expect, Carpet aligns the start of the grids
at $x=y=z=0$ and extends the ghost zones out into negative $x$, as
shown in figure \ref{fig:carpet:cells}. However, Carpet independently
decides the beginning \textit{index} from which to begin counting grid
cells, which is refinement-level local and which may change based on
refinement level and Carpet's whims.

\begin{figure*}[tbh]
  \centering
  \includegraphics[width=\textwidth]{refinement-at-bdry}
  \caption{The cell structure near refinement boundaries in Carpet,
    shown for the first three levels in our toy problem. Each level
    has three ghost cells. One physical cell is shown but many more
    exist. Fake cells are shown in grey.}
  \label{fig:carpet:cells}
\end{figure*}

At each refinement level, Carpet chooses one ``fiducial'' cell from
which to begin counting the cells at that level. On the coarsest
level, this is always the first cell. But on the finer levels it may
not be. For example, in figure \ref{fig:carpet:cells}, the first ghost
cell begins at index 2. The cells at index 0 and 1 do not really exist
and so we call them ``fake'' cells.

To tell us where the fiducial ``first cell'' resides on refined
levels, Carpet provides a parameter 
\begin{lstlisting}
  offset
\end{lstlisting}
which we call $\text{OF}_i$ for the $i^{\text{th}}$
level. $\text{OF}_i$ tells us the displacement (in cell widths at the
$i^{\text{th}}$ level) between the cell centre of the first cell at
level $i-1$ and the cell centre at level $i$. 

To help ourselves use this parameter, we define the parameter
\begin{lstlisting}
  gridstart
\end{lstlisting}
or $G_i$ for grid $i$, which defines the distance (in cell widths at
level $i$) between the centre of the fiducial first cell and the left
edge of the first cell of the coarsest grid (ghost or not). So grid 0
always has $G_0 = 0.5$.

The \texttt{gridstart} parameter for level $i$ obeys the following formula
\begin{equation}
  \label{eq:gridstart}
  G_i = G_{i-1}*f_i + \text{OF}_i,
\end{equation}
where $f_i$ is the refinement factor between level $i$ and level
$i-1$. For now we assume that
$$f_i = 2\ \forall\ i.$$

To tell us how many fake cells exist between the fiducial first grid
cell at each level and the first ghost cell, Carpet provides the
\begin{lstlisting}
  region
\end{lstlisting}
parameter, which we call $R_i$. The region parameter tells us the
index, with respect to the first fiducial cell, that the first cell
with allocated memory (ghost or not) begins. For example, in figure
\ref{fig:carpet:cells}, 
\begin{equation}
  \label{eq:regions}
  \begin{aligned}
    R_0 &= 0\\
    R_1 &= 2\\
    R_2 &= 7.
  \end{aligned}
\end{equation}

Carpet also tells us the index, with respect to the fiducial first
cell at level $i$, that the active region starts. This parameter,
perhaps unsurprisingly, is called
\begin{lstlisting}
  active
\end{lstlisting}
or $A_i$. In our case, since the active regions all start at $x=0$ and
we have 3 ghost cells,
$$A_i = R_i + 3\ \forall\ i,$$
but this isn't necessarily so.

Putting this all together, we have that the start index of the active
region of each grid, with respect to the left edge of the coarsest
grid, is
\begin{equation}
  \label{eq:start:index}
  S_i := G_i + A_i - 0.5,
\end{equation}
where the factor of $0.5$ is the move from cell-centres to cell edges.

\section{The Problem}
\label{sec:problem}

I am having difficulty figuring out how to pass all this information
to yt. I am currently setting the start index of each grid to $S_i$. I
am (naively) setting all cells at level 0 to be the parents of all
cells at level 1, etc. But for this example, that shouldn't matter.

I can safely perform slice plots, and the grids appear in the proper
place. However, when I perform projection plots, my grids appear
offset, and perhaps shrunken, as shown in figure
\ref{fig:projection:plot}.

\begin{figure}[tbh]
  \centering
  \includegraphics[width=\columnwidth]{grid_level_projection}
  \caption{A projection plot of the grid levels in yt using the grid
    structure described. There is an offset in the grid bounds, which
    I do not understand. The x and y axes are wrong, but this is a
    separate issue.}
  \label{fig:projection:plot}
\end{figure}

\section{Help?}
\label{sec:help}

If you have any suggestions, I would appreciate any help you can
offer. My preliminary attempt at a frontend can be found here:

\url{https://bitbucket.org/yt_analysis/yt/pull-requests/2121/wip-simulationio-frontend}

and I have prepared test data here:

\url{https://bitbucket.org/Yurlungur/simulationio-yt-tests/overview}


\end{document}