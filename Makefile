# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-04-17 04:09:31 (jmiller)>

all: refinement-at-bdry.pdf\
     carpet-refinement-problem.pdf

%.pdf: %.tex
	pdflatex $*
	pdflatex $*
	pdflatex $*

refinement-at-bdry.pdf: refinement-at-bdry.tex
	pdflatex $^ $@

clean:
	$(RM) *.aux *.log *.out refinement-at-bdry.pdf carpet-refinement-problem.pdf *Notes.bib

dummy: all clean
